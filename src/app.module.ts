import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MembersModule } from './members/members.module';
import { MongooseModule } from '@nestjs/mongoose';
import { from } from 'rxjs';

@Module({
  imports: [MembersModule,MongooseModule.forRoot('mongodb+srv://vikram:gSK163whYDOJlH3k@cluster0.otd8y.mongodb.net/membership?retryWrites=true&w=majority')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
