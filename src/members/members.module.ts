import { MongooseModule } from '@nestjs/mongoose';
import { Module } from "@nestjs/common";

import { MemberSchema } from './members.model';
import { MemberController } from './members.controller';
import { MembersService } from './members.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Member', schema: MemberSchema }])],
    controllers: [ MemberController ],
    providers: [ MembersService ]
})
export class MembersModule{}