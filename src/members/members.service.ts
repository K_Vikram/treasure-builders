import { MemberSchema, Member } from './members.model';
import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class MembersService{
    private members : Member[] = [];
    constructor(@InjectModel('Member') private readonly memberModel: Model<Member>){}
    async addNew(name: string, mobNo: number, email: string){
        const newMember = new this.memberModel({ name: name, mobNo: mobNo, email});
        await newMember.save();
    }
}
