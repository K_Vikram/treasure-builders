import { Controller, Body, Post } from "@nestjs/common";

import { MembersService } from './members.service';

@Controller()
export class MemberController{
    constructor(private readonly memberService: MembersService){}

    @Post()
    async addMember(
        @Body('name') memName: string,
        @Body('mobNo') memMob: number,
        @Body('email') memMail: string
    ){
        await this.memberService.addNew(memName, memMob, memMail); 
    }
}