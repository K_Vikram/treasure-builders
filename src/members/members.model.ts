import * as mongoose from 'mongoose';

export const MemberSchema = new mongoose.Schema({
    name: { type: String, requied: true },
    mobNo: { type: Number, required: true },
    email: { type: String, requied: true }
});
export interface Member extends mongoose.Document{
    id : string;
    name : string;
    mobNo : number;
    email : string;
}